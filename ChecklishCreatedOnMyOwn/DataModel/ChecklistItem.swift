//
//  ChecklistItem.swift
//  ChecklishCreatedOnMyOwn
//
//  Created by Artur on 3/13/18.
//  Copyright © 2018 Artur. All rights reserved.
//

class ChecklistItem {
    
    // MARK: - Propeties
    
    var text: String
    var isChecked: Bool
    
    // MARK: - Initialization
    
    init(text: String, isChecked: Bool) {
        self.text = text
        self.isChecked = isChecked
    }
    
    convenience init(text: String) {
        self.init(text: text, isChecked: false)
    }
    
    convenience init() {
        self.init(text: "", isChecked: false)
    }
    
    // MARK: - Methods
    
    func toggleCheckmark() {
        isChecked = !isChecked
    }
    
}

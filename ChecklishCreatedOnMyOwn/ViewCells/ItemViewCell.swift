//
//  ItemViewCell.swift
//  ChecklishCreatedOnMyOwn
//
//  Created by Artur on 3/13/18.
//  Copyright © 2018 Artur. All rights reserved.
//

import UIKit

var myTransition: CATransition = {
    let transition = CATransition()
    transition.type = kCATransitionFade
    transition.duration = 0.1
    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
    return transition
}()

class ItemViewCell: UITableViewCell {
    
    // MARK: - Properties
    
    var item = ChecklistItem() {
        didSet {
            configureItemTextLabel()
            configureCheckmarkLabel()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet weak var checkmarkLabel: UILabel!
    @IBOutlet weak var itemTextLabel: UILabel!
    
    // MARK: - Methods
    
    override func awakeFromNib() {
        configureItemTextLabel()
        configureCheckmarkLabel()
    }
    
    func configureItemTextLabel() {
        itemTextLabel.text = item.text
        self.layer.add(myTransition, forKey: nil)
    }
    
    func configureCheckmarkLabel() {
        checkmarkLabel.text = item.isChecked ? "✓" : ""
        self.layer.add(myTransition, forKey: nil)
    }
}


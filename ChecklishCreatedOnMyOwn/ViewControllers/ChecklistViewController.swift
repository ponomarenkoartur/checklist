//
//  ChecklistViewController.swift
//  ChecklishCreatedOnMyOwn
//
//  Created by Artur on 3/13/18.
//  Copyright © 2018 Artur. All rights reserved.
//

import UIKit

class ChecklistViewController: UITableViewController {

    var dataModel: DataModel!
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        let item0 = ChecklistItem(text: "Learn German")
        let item1 = ChecklistItem(text: "Do OS lab", isChecked: true)
        let item2 = ChecklistItem(text: "Improve project")
        dataModel.items = [
            item0,
            item1,
            item2
        ]
    }

    // MARK: - UITableViewDataSouce

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = dataModel.items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemViewCell
        cell.item = item
        return cell
    }

    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = dataModel.items[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath) as! ItemViewCell
        item.toggleCheckmark()
        cell.item = item
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            dataModel.items.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}
